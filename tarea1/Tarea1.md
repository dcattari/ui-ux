# Tarea 1

Diego Cattarinich Clavel

## Describa brevemente el software y su función principal.

GitLab es una plataforma Web/CLI que permite gestionar, administrar y mantener repositorios de archivos o proyectos ya sean personales o colaborativos.

## Describa la acción que usted como usuario intentaba realizar y la dificultad que experimentó.

Al crear un repositorio para esta tarea, desde la página web quiero crear una carpeta para ordenar mis archivos MarkDown de mejor manera. La dificultad es que no existe un botón que me permita hacerlo desde la web, solamente existen las opciones para poder agregar archivos y otras configuraciones. Por lo tanto tuve que clonar el repositorio para poder crear una carpeta y actualizar el repositorio para lograr tal simple acción.

## En su opinión, ¿qué atributo de facilidad de uso es el más afectado?
El de la Eficiencia. Ya que hacer una tarea tan simple, requere de un entorno de computador y quizas desde otro dispositivo, el objetivo se vuelve imposible.

## ¿Por qué este atributo es importante para este sistema?
La Eficiencia es super importante para GitLab ya que es un software que se define como una "suite completa que permite gestionar" y si bien algo puede ser completo, es la eficiencia la que permite una gestión óptima.

## Explique cómo ha identificado el atributo
Lo he identificado al querer organizar el repositorio con directorios para mantener una mejor estructura.

## ¿Midió ese atributo? Explique cómo, y si no lo hizo mida ahora explicando cómo

Si. Tomando el tiempo extra en agregar una carpeta a un repositorio recientemente creado. El que tomó alrededor de 2 minutos (1 min 48 s). Considerando que tuve que clonar, agregar una carpeta y mover el archivo que quería a ella y volver a subir. Además, teniendo una cuenta de GitHub y no la de GitLab, tuve que añadir mis credenciales para hacerle "Push" a mi versión local. Esto para un usuario avanzado puede ser muy fácil, pero creo que para un usuario nuevo, aprender los comandos para lograr esto puede ser abrumador.